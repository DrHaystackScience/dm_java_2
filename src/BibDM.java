import java.util.Collections;
import java.util.List;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class BibDM{

    /**
     * Ajoute deux entiers
     * @param a le premier entier à ajouter
     * @param b le deuxieme entier à ajouter
     * @return la somme des deux entiers
     */
    public static Integer plus(Integer a, Integer b){
        return a + b;
    }


    /**
     * Renvoie la valeur du plus petit élément d'une liste d'entiers
     * VOUS DEVEZ LA CODER SANS UTILISER COLLECTIONS.MIN (i.e. vous devez le faire avec un for)
     * @param liste
     * @return le plus petit élément de liste
     */
    public static Integer min(List<Integer> liste){
        Integer res;
        if(liste.size()>0)
            res = liste.get(0);
        else
            res = null;
        for(Integer nb : liste){
            if(nb < res)
                res = nb;
        }
        return res;
    }
    
    
    /**
     * Teste si tous les élements d'une liste sont plus petits qu'une valeure donnée
     * @param valeur 
     * @param liste
     * @return true si tous les elements de liste sont plus grands que valeur.
     */
    public static<T extends Comparable<? super T>> boolean plusPetitQueTous( T valeur , List<T> liste){
        boolean res = true;
        int indice = 0;
        while(indice < liste.size() && res){
            if(liste.get(indice).compareTo(valeur) <= 0)
                res =false;
            indice++;
        }
        return res;
    }



    /**
     * Intersection de deux listes données par ordre croissant.
     * @param liste1 une liste triée
     * @param liste2 une liste triée
     * @return une liste triée avec les éléments communs à liste1 et liste2
     */
    public static <T extends Comparable<? super T>> List<T> intersection(List<T> liste1, List<T> liste2){
        return null;
    }


    /**
     * Découpe un texte pour obtenir la liste des mots le composant. texte ne contient que des lettres de l'alphabet et des espaces.
     * @param texte une chaine de caractères
     * @return une liste de mots, correspondant aux mots de texte.
     */
    public static List<String> decoupe(String texte){
        List<String> res = new ArrayList();
        String[] temp = texte.split(" ");
        for(String mot : temp){
            if(!mot.equals(" ") && !mot.equals(""))
                res.add(mot);
        }
        return res;
    }


    /**
     * Renvoie le mot le plus présent dans un texte.
     * @param texte une chaine de caractères
     * @return le mot le plus présent dans le texte. En cas d'égalité, renvoyer le plus petit dans l'ordre alphabétique
     */

    public static String motMajoritaire(String texte){
        String res = "";
        Integer max;
        max = null;

        if(texte.length() > 0){
        
            List<String> liste = decoupe(texte);
            List<String> tempRes = new ArrayList<>();
            Map<String, Integer> dico = new HashMap<>();

            for(String mot : liste){
                if(!dico.containsKey(mot))
                    dico.put(mot, 1);
                else
                    dico.put(mot, dico.get(mot) + 1);
            }

            for(String key : dico.keySet()){
                if(max==null || dico.get(key) > max)
                    max = dico.get(key);
            }

            for(String key : dico.keySet()){
                if(dico.get(key) == max){
                    tempRes.add(key);
                }
            }

            res = Collections.min(tempRes);
        }else{
            res = null;
        }

        return res;
    }
    
    /**
     * Permet de tester si une chaine est bien parenthesée
     * @param chaine une chaine de caractères composée de ( et de )
     * @return true si la chaine est bien parentjèsée et faux sinon. Par exemple ()) est mal parenthèsée et (())() est bien parenthèsée.
     */
    public static boolean bienParenthesee(String chaine){
        boolean res = true;
        int cpt = 0;
        int longueur = chaine.length();

        for(int i = 0; i < longueur; i++){
            if(chaine.charAt(i) == '(')
                cpt++;
            if(chaine.charAt(i) == ')')
            {
                if(cpt==0){
                    res = false;
                }
                if(cpt>0){
                    cpt--;
                }
            }     
        }
        
        if(cpt == 0 && res)
            res = true;
        else
            res = false;
        return res;
    }
    
    /**
     * Permet de tester si une chaine est bien parenthesée
     * @param chaine une chaine de caractères composée de (, de  ), de [ et de ]
     * @return true si la chaine est bien parentjèsée et faux sinon. Par exemple ([)] est mal parenthèsée alors ue ([]) est bien parenthèsée.
     */
    public static boolean bienParentheseeCrochets(String chaine){
        boolean res = true;
        int cptPar = 0;
        int cptCro = 0;
        int reperePar = 0;
        int repereCro = 0;
        int longueur = chaine.length();

        for(int i = 0; i < longueur; i++){
            if(chaine.charAt(i) == '(')
            {
                repereCro = cptCro;
                cptPar++;
            }
            if(chaine.charAt(i) == ')')
            {
                if(cptPar==0){
                    res = false;
                }
                if(cptPar>0){
                    if(cptCro != repereCro){
                        res = false;
                    }
                    else{
                        cptPar--;
                    }
                }
            }
            if(chaine.charAt(i) == '['){
                reperePar = cptPar;
                cptCro++;
            }
            if(chaine.charAt(i) == ']')
            {
                if(cptCro==0){
                    res = false;
                }
                if(cptCro>0){
                    if(cptPar != reperePar){
                        res = false;
                    }
                    else{
                        cptCro--;
                    }
                }
            }
        }

        
        if(cptPar == 0 && res && cptCro == 0)
            res = true;
        else
            res = false;
        return res;


    }


    /**
     * Recherche par dichtomie d'un élément dans une liste triée
     * @param liste, une liste triée d'entiers
     * @param valeur un entier
     * @return true si l'entier appartient à la liste.
     */
    public static boolean rechercheDichotomique(List<Integer> liste, Integer valeur){
        boolean res = false;
        int indiceDeb = 0;
        int indiceFin = liste.size() - 1;
        int mid = liste.size() / 2;

        while(indiceDeb <= indiceFin && !res){
            mid = (indiceDeb + indiceFin) / 2;
            if(liste.get(mid) == valeur){
                res = true;
            }else if(liste.get(mid) > valeur){
                indiceFin = mid - 1;
            }else{
                indiceDeb = mid + 1;
            }
        }
        return res;
    }



}
